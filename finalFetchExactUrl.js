const re2 = require("re2")
const urlRegex = require('url-regex')
const fs = require('fs');
const { regExpList, replaceRegExpList, proxyURL } = require("../constants.js")
const { gettingTheHostNameFromURL, replaceAllString } = require('../common-util');
const whiteList = require("../config.json")

const regex1 = new re2(/[^("|')]+/) // to remove the routes
var cnt = 0

filterURL = (url) => {
    url = regex1.exec(url);
    return url[0]
}

fs.readFile('./test.html', 'utf8', (err, data) => {
    if (err) {
        console.error(err);
        return;
    }
    urls = data.match(urlRegex())
    urls = urls.map(filterURL)
    urls = urls.sort((a, b) => b.length - a.length);
    for (let i of urls) {
        originalURL = i.slice()

        domain = gettingTheHostNameFromURL(i)
        var result = whiteList.includes(domain)

        if (result) {
            replacedDomain = domain
            for (k in replaceRegExpList) {
                replacedDomain = replaceAllString(replacedDomain, replaceRegExpList[k], regExpList[k])
            }
            i = replaceAllString(i, "https", "http")
            i = i.replace(domain, replacedDomain + "." + proxyURL)
            data = replaceAllString(data, originalURL, i)
        }
    }
    console.log(data)

});