const re2 = require("re2")
const urlRegex = require('url-regex')
const fs = require('fs');
const { regExpList, replaceRegExpList, proxyURL } = require("./constants.js")
const { replaceAll, gettingTheHostNameFromURL,replaceAllString } = require('./common-util');
const whiteList = require("./config.json")


const regex1 = new re2(/[^("|')]+/) // to remove the routes
var cnt = 0



fs.readFile('./other/test.html', 'utf8', (err, data) => {
    if (err) {
        console.error(err);
        return;
    }

    urls = data.match(urlRegex())
    originalURLS = []
    replacedURLS = []
    for (let i of urls) {

        i = regex1.exec(i)

        domain = gettingTheHostNameFromURL(i[0])
        // console.log(domain)
        var result = whiteList.includes(domain)
        if (result) {
            originalURLS.push(i[0])

            replacedDomain = domain
            for (k in replaceRegExpList) {
                replacedDomain = replaceAll(replacedDomain, replaceRegExpList[k], regExpList[k])
            }



            i[0] = i[0].replace("https", "http")
            i[0] = i[0].replace(domain, replacedDomain + "." + proxyURL)
            replacedURLS.push(i[0])
        }
    }


    originalURLS = new Set(originalURLS)
    replacedURLS = new Set(replacedURLS)
    originalURLS = [...originalURLS]
    replacedURLS = [...replacedURLS]

    for (i in originalURLS) {
        data = replaceAllString(data, originalURLS[i], replacedURLS[i])
    }
    console.log(data)


});