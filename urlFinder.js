const re2 = require("re2")
const fs = require('fs');
const regex1 = new re2(/((https?:)|(www)|(\/\/))[^("|'|\s)]+/g)
var cnt = 0

fs.readFile('./other/test.html', 'utf8', (err, data) => {
    if (err) {
        console.error(err);
        return;
    }
    while ((ans = regex1.exec(data)) !== null) {
        cnt = cnt + 1
        console.log(`URL ${cnt}: ${ans[0]}`)
    }
});

