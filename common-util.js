const RE2 = require("re2");
const domainList = require("./config.json")

pattern = "("
for (i of domainList) {
  pattern = pattern + "(" + i + ")|"
}
pattern = pattern.slice(0, -1)
pattern = pattern + ")"
// console.log(pattern)
const whiteList = new RE2(pattern)

function gettingTheHostNameFromURL(websiteURL) {
  var getTheHostName;
  if (websiteURL.indexOf("//") > -1) {
    getTheHostName = websiteURL.split('/')[2];
  } else {
    getTheHostName = websiteURL.split('/')[0];
  }
  getTheHostName = getTheHostName.split(':')[0];
  getTheHostName = getTheHostName.split('?')[0];
  return getTheHostName;
}



const escapeRegExp = (string) => {
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}


function replaceAll2(str, find, replace) {
  var escapedFind=find.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
  return str.replace(new RegExp(escapedFind, 'g'), replace);
}




const replaceAll1 = (str, match, replacement) => {
  // return str.replace(new RegExp(escapeRegExp(match), 'g'), ()=>replacement);
  return str.replace(new RE2('\\b' + match + '\\b', 'g'), () => replacement);
}

const replaceAllString = (str, match, replacement) => {
  // return str.replace(new RegExp(escapeRegExp(match), 'g'), ()=>replacement);
  return str.replace(new RegExp(escapeRegExp(match), 'g'), () => replacement);
}

// const replaceAllString2 = (str, match, replacement) => {
//   return str.replace(new RegExp(escapeRegExp(match), 'g'), () => replacement);
// }

module.exports = { escapeRegExp, replaceAll1, gettingTheHostNameFromURL, whiteList,replaceAllString,replaceAll2 };
