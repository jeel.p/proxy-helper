const re2 = require("re2")
const urlRegex = require('url-regex')
const fs = require('fs');

const regex1 = new re2(/[^("|')]+/)
var cnt = 0

check = [
    "pyther.com",
    "www.pyther.com",
    "proxy-test01.pyther.com",
    "proxy-test02.pyther.com",
    "nodeapi.pyther.com",
    "use.fontawesome.com",
    "www.sciencedirect.com",
    "els-cdn.com",
    "elsevier.com",
    "pdf.sciencedirectassets.com",
    "sdfestaticassets-eu-west-1.sciencedirectassets.com",
    "doi.org",
    "sdfestaticassets-us-east-1.sciencedirectassets.com"
]


pattern = "/("
for (i of check) {
    pattern = pattern + "(" + i + ")|"
}
pattern = pattern.slice(0, -1)
pattern = pattern + ")/"

const regex2 = new re2(pattern)
console.log(pattern)



fs.readFile('./other/test.html', 'utf8', (err, data) => {
    if (err) {
        console.error(err);
        return;
    }


    ans = data.match(urlRegex())

    for (i of ans) {
        cnt = cnt + 1
        i = regex1.exec(i)
        console.log(`url ${cnt}: ${i[0]}  ${regex2.test(i[0])}`)
    }
});

