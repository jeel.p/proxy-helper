const regExpList = ['--', '-', '>']
const replaceRegExpList = ['-', '.', '/']
const proxyURL = "proxy.pyther.com"

module.exports = {regExpList, replaceRegExpList, proxyURL}